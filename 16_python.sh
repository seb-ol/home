#!/bin/bash

if [[ $UID -ne 0  ]]; then
	export PATH="/home/$SYS_HOME_USER/.pyenv/bin:$PATH"
	eval "$(pyenv init -)"
	eval "$(pyenv virtualenv-init -)"
fi

