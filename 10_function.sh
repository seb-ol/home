#!/usr/bin/env bash

s() {
    if [[ $# == 0 ]]; then
        sudo $(history -p '!!')
    else
        sudo "$@"
    fi
}

function extract () {
    if [ -f $1 ] ; then
      case $1 in
        *.tar.bz2)   tar xjf $1     ;;
        *.tar.gz)    tar xzf $1     ;;
        *.bz2)       bunzip2 $1     ;;
        *.rar)       unrar e $1     ;;
        *.gz)        gunzip $1      ;;
        *.tar)       tar xf $1      ;;
        *.tbz2)      tar xjf $1     ;;
        *.tgz)       tar xzf $1     ;;
        *.zip)       unzip $1       ;;
        *.Z)         uncompress $1  ;;
        *.7z)        7z x $1        ;;
        *)     echo "'$1' cannot be extracted via extract()" ;;
         esac
     else
         echo "'$1' is not a valid file"
     fi
}

function mcd() {
  mkdir -p "$1" && cd "$1";
}

function tarbz(){

    if [[ ! -z $2 ]]; then
        name=$(echo ${2} | sed 's/\/$//' )
        cmd="${1}.tar.bz2 ${name}"
    else
        name=$(echo ${1} | sed 's/\/$//' )
        cmd="${name}.tar.bz2 ${name}"
    fi
    echo "tar $cmd"
    tar cjf $cmd
}

function targz(){

    if [[ ! -z $2 ]]; then
        name=$(echo ${2} | sed 's/\/$//' )
        cmd="${1}.tar.gz ${name}"
    else
        name=$(echo ${1} | sed 's/\/$//' )
        cmd="${name}.tar.gz ${name}"
    fi
    echo "tar $cmd"
    tar czf $cmd
}

function gpu(){
    out=$(git push 2>&1 | awk '/set-upstream/ {print $4" "$5}')

    if [[ ! -z $out ]]; then
        echo "${out}"
        git push --set-upstream ${out}
    fi



}
alias reload_bash='source ~/.bashrc'