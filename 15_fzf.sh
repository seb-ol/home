# Setup fzf
# ---------

#FZF_HOME=$(echo "$(dirname "$(readlink -f "${BASH_SOURCE[0]}")")" | awk -F "/" '{for (x=1;x<=NF;x++) if ($x~"home") print $(x+1) }')

if [[ ! "$PATH" == */home/$SYS_HOME_USER/.fzf/bin* ]]; then
  export PATH="$PATH:/home/$SYS_HOME_USER/.fzf/bin"
fi

# Auto-completion
# ---------------
[[ $- == *i* ]] && source "/home/$SYS_HOME_USER/.fzf/shell/completion.bash" 2> /dev/null

# Key bindings
# ------------


source "/home/$SYS_HOME_USER/.fzf/shell/key-bindings.bash"

#[ -f ~/.fzf.bash ] && source ~/.fzf.bash
#
