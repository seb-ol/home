#!/usr/bin/env bash
[ -x /usr/bin/lesspipe ] && eval "$(SHELL=/bin/sh lesspipe)"
if [ -x /usr/bin/dircolors ]; then
    test -r ~/.dircolors && eval "$(dircolors -b ~/.dircolors)" || eval "$(dircolors -b)"
    alias ls='ls --color=auto'
    alias grep='grep --color=auto'
    alias fgrep='fgrep --color=auto'
    alias egrep='egrep --color=auto'
fi



source /usr/lib/git-core/git-sh-prompt

export GIT_PS1_SHOWDIRTYSTATE=1 GIT_PS1_SHOWSTASHSTATE=1 GIT_PS1_SHOWUNTRACKEDFILES=1 GIT_PS1_SHOWCOLORHINTS=1
export GIT_PS1_SHOWUPSTREAM=verbose GIT_PS1_DESCRIBE_STYLE=branch

if [[ $UID -eq 0  ]]; then
# retour de commande a chaque ligne
#   export PS1='\[\033[01;31m\]\u\[\033[01;33m\]@\[\033[01;36m\]\h \[\033[01;33m\]\w \[\033[01;35m\]\$ \[\033[00m\]'
export PS1='\[\e[0;1;$((31+($?==0)))m\] $? \[\e[37m\]| \[\033[01;31m\]\u\[\033[01;33m\]@\[\033[01;36m\]\h \[\033[01;33m\]\w\n \[\033[01;037m\]\D{%d/%m/%y} \t $! $(__git_ps1) \[\033[01;35m\]\$ \[\033[00m\]'
else
#    export PS1='\[\033[01;32m\]\u\[\033[01;33m\]@\[\033[01;36m\]\h \[\033[01;33m\]\w \[\033[01;35m\]\$ \[\033[00m\]'
export PS1='\[\e[0;1;$((31+($?==0)))m\] $? \[\e[37m\]| \[\033[01;32m\]\u\[\033[01;33m\]@\[\033[01;36m\]\h \[\033[01;33m\]\w\n \[\033[01;037m\]\D{%d/%m/%y} \t $! $(__git_ps1) \[\033[01;35m\]\$ \[\033[00m\]'
fi
export PS2='\[\033[1m\]>\[\033[m\] '

export PATH=$PATH:/opt/scripts:/sbin:/usr/sbin:/usr/loca/bin
export VISUAL=/usr/bin/vim
export EDITOR=vim

alias vi='vim'
alias cd..='cd ..'
alias .='cd -'
alias cd.='cd -'
alias ..='cd ..'
alias ...='cd ../../'
alias ....='cd ../../../'
alias wget='wget --content-disposition'
alias du='du -h'
alias df='df -h'
alias mkdir='mkdir -p'
alias sudo='sudo -s'
alias cleanvim='rm *~ && rm ~/.vim/backswap/.* && rm ~/.vim/backswap/*'
alias getip='curl icanhazip.com'
alias less='less -R'


export LESS_TERMCAP_mb=$'\E[01;31m'    # debut de blink !
export LESS_TERMCAP_md=$'\E[01;31m'    # debut de gras
export LESS_TERMCAP_me=$'\E[0m'        # fin
export LESS_TERMCAP_so=$'\E[01;44;33m' # début de la ligne d'état
export LESS_TERMCAP_se=$'\E[0m'        # fin
export LESS_TERMCAP_us=$'\E[01;32m'    # début de souligné
export LESS_TERMCAP_ue=$'\E[0m'        # fin

export LS_OPTIONS="--human --color=always"
alias ll='ls $LS_OPTIONS -alF'
alias la='ls $LS_OPTIONS -A'
alias lf='ls $LS_OPTIONS -Gl | grep ^d'
alias l.='ls $LS_OPTION -d .*'
alias lfr='ls $LS_OPTION -R | grep ":$" | sed -e '\''s/:$//'\'' -e '\''s/[^-][^\/]*\//--/g'\'' -e '\''s/^/   /'\'' -e '\''s/-/|/'\'''
alias l='ls $LS_OPTIONS -Al'
alias dir='ls'

alias gs='git status'

if [ -f ~/.bash_aliases ]; then
    . ~/.bash_aliases
fi


function bashrc() {
  echo 'if [ -d ~/.bashrc.d ]; then
  cd ~/.bashrc.d
  for file in $(find . -name \*.sh); do
    source $file
  done
  cp .vimrc ~/.vimrc
  cd - > /dev/null
fi
' >> ~/.bashrc

}