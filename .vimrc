"==============================
"======= Partie Général =======
"==============================

set nocompatible
set nowrap
set noloadplugins

runtime! debian.vim
if has("syntax")
  syntax on
endif
set background=light

filetype on
filetype plugin on
filetype indent on

set updatetime=4200
set encoding=utf-8
set showcmd		" Show (partial) command in status line.
set showmatch		" Show matching brackets.
set ignorecase		" Do case insensitive matching
set smartcase		" Do case insensitive matching
set autowrite		" Automatically save before commands like :next and :make
set number 		" Affiche les lignes
set history=200		" retien 200 ligne 
set autoread 		" recahrge le fichier si modifier de l'exterieur
set hlsearch 		" Highlight search things
set incsearch "Make search act like search in modern browsers
set encoding=utf8 " encodage utf8
set visualbell                          " Silence the bell, use a flash instead"
set lbr " word-wrap " retour a la ligne au lieux de couper un mot en deux
set ai "Auto indent
set si "Smart indet
set wrap "Wrap lines  pas compris le fonctionnement
set linebreak
set modeline
set nostartofline
set t_Co=256
set title
set virtualedit=onemore " permet de se placer après la fin de ligne en mode normal
"set paste
set expandtab
set smarttab
"set foldmethod=marker

" Cool tab completion stuff
set wildmenu
set wildmode=list:longest,full


" Leader
"let mapleader = "%"
"let g:mapleader = "%"



" ################################################
" ############################# Experimental #####
" ################################################

set backspace=indent,eol,start
set showmode

"set mouse=a
set cindent " indentation comme en standars c
"set cursorcolumn " affiche la colonne : un peu chiant quand meme 

set cul " affiche la ligne 
au BufwinLeave * silent! mkview " se repositioner ou on etait dans le fichier
au BufwinEnter * silent! loadview
set writeany " permet de se passer de ! pour !wq
set ttyfast
set scrolloff=5 " garde au moins 5 line at extremity of doc
set sidescrolloff=3 " pareil mais a l'horizontal (parait que c'est mieux)
set magic " a verifier , rapport avec les expression rationel
set mat=1 " temps d'affichage des acolade
set noerrorbells

if exists('+autochdir')
    set autochdir
else
    autocmd BufEnter * silent! lcd %:p:h:gs/ /\\ /
endif

" ################################################
" ######################## Fin  Experimental #####
" ################################################

" ################################################
" ################# Function #####################
" ################################################



" ########################### Affichage status line
if has ('statusline')
	set laststatus=2
	"set statusline=%<%f\   " Filename
	set statusline=%w%h%m%r " Options
	set statusline+=\ [%{&ff}/%Y]            " filetype
	set statusline+=\ [%{getcwd()}/
	set statusline+=%f] " current dir " fichier
	set statusline+=%=          " left / right sépareteur
	set statusline+=%#warningmsg#
	set statusline+=%{(&fenc!='utf-8'&&&fenc!='')?'['.&fenc.']':''}
	set statusline+=%*
	set statusline+=[%{strlen(&fenc)?&fenc:'none'}]
	set statusline+=[%c-%l/%L]          " placement dans le fichier
        
endif
set showtabline=2
" ############################ Backup File ! 
set backup
if filewritable(expand("~/.vim/backup")) == 2
	set backupdir=$HOME/.vim/backup
	else
	if has("unix") || has("win32unix")
		call system("mkdir -p $HOME/.vim/backup")
		set backupdir=$HOME/.vim/backup
	endif
endif

if filewritable(expand("~/.vim/backswap")) == 2
	set directory=$HOME/.vim/backswap
	else
	if has("unix") || has("win32unix")
		call system("mkdir -p $HOME/.vim/backswap")
		set directory=$HOME/.vim/backswap
	endif
endif

if filewritable(expand("~/.vim/backviews")) == 2
	set viewdir=$HOME/.vim/backviews
	else
	if has("unix") || has("win32unix")
		call system("mkdir -p $HOME/.vim/backviews")
		set viewdir=$HOME/.vim/backviews
	endif
endif
" ########################## Fin de selection des dossier de backup 

" indetation

set expandtab                   " expand <Tab>s with spaces; death to tabs!
set shiftwidth=4                " spaces for each step of (auto)indent
set softtabstop=4               " set virtual tab stop (compat for 8-wide tabs)
set tabstop=8                   " for proper display of files with tabs
set shiftround                  " always round indents to multiple of shiftwidth
set copyindent                  " use existing indents for new indents
set preserveindent              " save as much indent structure as possible

" =================================
" ========= Partie Mappage ========
" =================================

function! s:VSetSearch()
    let temp = @@
    norm! gvy
    let @/ = '\V' . substitute(escape(@@, '\'), '\n', '\\n', 'g')
    let @@ = temp
endfunction
vnoremap * :<C-u>call <SID>VSetSearch()<CR>//<CR>
vnoremap # :<C-u>call <SID>VSetSearch()<CR>??<CR>
nnoremap # :<C-m> l:Matrix<CR>


inoremap <silent> <<space> <><left>

"==============================
"======= Partie Program =======
"==============================

function ModeChange()
    exec 'w! %'
    if getline(1) =~ "^#!"
        if getline(1) =~ "/bin/"
            silent !chmod +x <afile>
        endif
    endif
endfunction

"augroup script
"       au!
"       autocmd! BufNewFile,BufReadPre,FileReadPre  *.py    so ~/.vim/conf/python.vim
"       autocmd! BufNewFile,BufReadPre,FileReadPre  *.pl    so ~/.vim/conf/perl.vim
"       autocmd! BufNewFile,BufReadPre,FileReadPre  *.sh,*.bash    so ~/.vim/conf/bash.vim
"       autocmd! BufNewFile,BufReadPre,FileReadPre  *.php    so ~/.vim/conf/php.vim
"       autocmd! BufNewFile,BufReadPre,FileReadPre  *.yml,*.yaml    so ~/.vim/conf/yaml.vim
"augroup END


"==============================
"======= Partie Plugins =======
"==============================
"source ~/.vim/plugin/supertab.vim
"source ~/.vim/plugin/comments.vim
"source ~/.vim/plugin/human.vim
"source ~/.vim/plugin/autoclose.vim
"source ~/.vim/plugin/matrix.vim
"source ~/.vim/plugin/ragtag.vim
"let g:SuperTabDefaultCompletionType = "<C-X><C-O>"
"let g:SuperTabDefaultCompletionType = "context"
"let g:loaded_matchparen=0

"call pathogen#infect()

